const APIKEY = '7d41393317f37bf50f788143373eee7d';



function geo(position) {
    var lat = position.coords.latitude;
    var long = position.coords.longitude;
    document.getElementById('latitude').innerHTML = lat;
    document.getElementById('longitude').innerHTML = long;
    let geol = `https://api.openweathermap.org/data/2.5/weather?q=${position}&appid=${APIKEY}&units=metric&lang=fr`;

}
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(geo);
}








let apiRequest = function (city) {
let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APIKEY}&units=metric&lang=fr`;

    fetch(url).then((Response) =>
        Response.json().then((data) => {
            console.log(data);
            document.querySelector('#temp').innerHTML = data.main.feels_like + "°";
            document.querySelector('#tempMax').innerHTML = data.main.temp + "°";
            document.querySelector('#tempMin').innerHTML = data.main.temp_min + "°";
            document.querySelector('#humidity').innerHTML = data.main.humidity + "% d'humiditée dans l'air.";
            document.querySelector('#wind').innerHTML = data.wind.speed + " m/s";
            document.querySelector('#windDeg').innerHTML = data.wind.deg + "°";
            document.querySelector('#lati').innerHTML = data.coord.lat ;
            document.querySelector('#lng').innerHTML = data.coord.lon ;
            
        })
    );
   } 
    document.querySelector('form').addEventListener('submit', function (e) {
        e.preventDefault();
        let ville = document.querySelector('#inputVille').value
        apiRequest(ville);
    });
   